package br.com.geoambiente.fieldcollector.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import br.com.geoambiente.fieldcollector.R;
import br.com.geoambiente.fieldcollector.domain.InspecaoCpfl;
import br.com.geoambiente.fieldcollector.interfaces.RecycleViewOnClickListenerHack;

/**
 * Created by mateus on 09/07/2015.
 */
public class InspecaoAdapter extends RecyclerView.Adapter<InspecaoAdapter.MyViewHolder> {
    private List<InspecaoCpfl> mList;
    private LayoutInflater mLayoutInflater;
    private RecycleViewOnClickListenerHack mRecycleViewOnClickListenerHack;
    private boolean onBind;

    public InspecaoAdapter(Context context, List<InspecaoCpfl>l){
        mList = l;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = mLayoutInflater.inflate(R.layout.item_inpecao_cpfl, viewGroup, false);

        MyViewHolder mvh = new MyViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int position) {

        onBind = true;

        myViewHolder.torre.setText("Torre: " + mList.get(position).torre);
        myViewHolder.municipio.setText("Município: " + mList.get(position).municipio);
        myViewHolder.data_inspecao.setText("Data Inspeção: " + mList.get(position).dataInspecao);


        if(mList.get(position).dataEnvio == null) {
            myViewHolder.data_envio.setTextColor(Color.RED);
            myViewHolder.data_envio.setText("Data Envio: aguardando envio");
        }
        else
            myViewHolder.data_envio.setText("Data Envio: " + mList.get(position).dataEnvio);

        onBind = false;
    }

    public void addListItem(InspecaoCpfl inspecao, int position)
    {
        mList.add(inspecao);
        notifyItemInserted(position);
    }

    public void removeListItem(int position){
        mList.remove(position);
        notifyItemRemoved(position);
    }

    public void removeAll(){
        int size = mList.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                removeListItem(0);
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public Object getItem(int position)
    {
        return mList.get(position);
    }

    public RecycleViewOnClickListenerHack getmRecycleViewOnClickListenerHack() {
        return mRecycleViewOnClickListenerHack;
    }

    public void setRecycleViewOnClickListenerHack(RecycleViewOnClickListenerHack mRecycleViewOnClickListenerHack) {
        this.mRecycleViewOnClickListenerHack = mRecycleViewOnClickListenerHack;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView torre;
        public TextView municipio;
        public TextView data_inspecao;
        public TextView data_envio;

        public MyViewHolder(View itemView) {
            super(itemView);

            torre = (TextView) itemView.findViewById(R.id.torre);
            municipio = (TextView) itemView.findViewById(R.id.municipio);
            data_inspecao = (TextView) itemView.findViewById(R.id.data_inspecao);
            data_envio = (TextView) itemView.findViewById(R.id.data_envio);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(mRecycleViewOnClickListenerHack != null) {
                mRecycleViewOnClickListenerHack.onClickListener(view, getPosition());
            }
        }
    }
}
