package br.com.geoambiente.fieldcollector.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import br.com.geoambiente.fieldcollector.MainActivity;
import br.com.geoambiente.fieldcollector.R;
import br.com.geoambiente.fieldcollector.adapters.InspecaoAdapter;
import br.com.geoambiente.fieldcollector.dao.InspecaoCpflService;
import br.com.geoambiente.fieldcollector.domain.DomainHelper;
import br.com.geoambiente.fieldcollector.domain.InspecaoCpfl;
import br.com.geoambiente.fieldcollector.interfaces.IEventFilter;
import br.com.geoambiente.fieldcollector.interfaces.IEventsMenu;
import br.com.geoambiente.fieldcollector.interfaces.RecycleViewOnClickListenerHack;
import br.com.geoambiente.fieldcollector.util.JsonUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListaInspecaoCPFLFragment extends Fragment implements RecycleViewOnClickListenerHack, IEventFilter, IEventsMenu {


    private RecyclerView mRecyclerView;
    private List<InspecaoCpfl> mList;
    private ProgressBar mProgressBar;
    private ProgressDialog mProgressDialog;
    private InspecaoCpfl mDomain;
    private String _query;
    private Context context;

    public ListaInspecaoCPFLFragment() {

    }


    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getActivity()).setEventsMenu(this);
        ((MainActivity)getActivity()).setEventFilter(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_lista_inspecao_cpfl, container, false);
        context = getActivity();

        mList = new ArrayList<InspecaoCpfl>();
        mDomain = new InspecaoCpfl();
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mProgressDialog = new ProgressDialog(getActivity());

        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_list);
        mRecyclerView.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(llm);


        mProgressDialog.setIcon(R.mipmap.geoambiente);
        mProgressDialog.setTitle("Aguarde");
        mProgressDialog.setMessage("Carregando Inspeções!");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        final Handler handler = new Handler();
        new Thread(new Runnable(){
            @Override
            public void run() {

                mList = (new InspecaoCpflService(getActivity())).getTorre(DomainHelper.mFilter);

                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        if(mList.size() > 0)
                            view.findViewById(R.id.imageCpfl).setVisibility(View.GONE);
                        //mList = ((MainActivity)getActivity()).getPropriedadeList();
                        carregaAdapter();
                        mProgressDialog.dismiss();
                    }
                });
            }
        }).start();

        return view;

        /*mList = (new InspecaoCpflService(getActivity())).getTorre(DomainHelper.mFilter);

        if(mList.size() > 0)
            view.findViewById(R.id.imageCpfl).setVisibility(View.GONE);
        //mList = ((MainActivity)getActivity()).getPropriedadeList();
        InspecaoAdapter adapter = new InspecaoAdapter(getActivity(), mList);
        adapter.setRecycleViewOnClickListenerHack(this);

        mRecyclerView.setAdapter(adapter);

        return view;*/
    }

    private void carregaAdapter(){
        InspecaoAdapter adapter = new InspecaoAdapter(getActivity(), mList);
        adapter.setRecycleViewOnClickListenerHack(this);
        mRecyclerView.setAdapter(adapter);
    }

    private void obtemItens() {

    }

    @Override
    public void onClickListener(View view, int position) {

        final InspecaoCpfl inspecao =
                (InspecaoCpfl) ((InspecaoAdapter) mRecyclerView.getAdapter()).getItem(position);

        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setIcon(R.mipmap.geoambiente);
        alert.setTitle("Inspeção de Campo");
        alert.setItems(new String[]{"Editar Torre", "Copiar Torre"}, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(which == 0){
                    ((MainActivity)getActivity()).changeInspecao(inspecao);
                }
                else{
                    inspecao.codigo = 0;
                    inspecao.dataEnvio = "";
                    inspecao.dataInspecao = "";
                    inspecao.foto = "";
                    ((MainActivity)getActivity()).changeInspecao(inspecao);
                }
            }
        });
        alert.show();
    }

    @Override
    public void filterTorre(String torre) {
        InspecaoAdapter adapter = (InspecaoAdapter) mRecyclerView.getAdapter();
        adapter.removeAll();

        //mRecyclerView.setAdapter(null);
    }

    @Override
    public void takePicture() {

    }

    @Override
    public void send() {
        new sendAsync().execute();
    }

    @Override
    public void save() {

    }

    private class ObtemInspecoes extends AsyncTask<Void, Void, Void>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            List<InspecaoCpfl> inspecoes = (new InspecaoCpflService(getActivity())).getAll();

            InspecaoAdapter adapter = (InspecaoAdapter) mRecyclerView.getAdapter();

            for(InspecaoCpfl prop : inspecoes)
                adapter.addListItem(prop, mList.size());

            return null;
        }

        @Override
        protected void onPostExecute(Void success) {
            super.onPostExecute(success);

            mProgressBar.setVisibility(View.GONE);
        }
    }

    private class sendAsync extends AsyncTask<Void, Void, Void>{

        private ProgressDialog mProgressDialog = new ProgressDialog(getActivity());

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog.setIcon(R.mipmap.geoambiente);
            mProgressDialog.setTitle("Aguarde");
            mProgressDialog.setMessage("Enviando as inspeções para a Geoambiente!");
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            InspecaoCpflService service = new InspecaoCpflService(getActivity());
            List<InspecaoCpfl> list = service.getPend();

            for(InspecaoCpfl inspecao : list)
            {
                try {
                    String url = "http://130.211.170.236/fieldcollector/MobileApps/PostInspecaoCpfl";

                    HttpParams headers = new BasicHttpParams();
                    HttpProtocolParams.setContentCharset(headers, "utf-8");

                    HttpClient httpClient = new DefaultHttpClient(headers);
                    HttpPost httpPost = new HttpPost(url);


                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                    nameValuePairs.add(new BasicNameValuePair("acaoCorretivaPreventiva", inspecao.acaoCorretivaPreventiva));
                    nameValuePairs.add(new BasicNameValuePair("dataInspecao", inspecao.dataInspecao));
                    nameValuePairs.add(new BasicNameValuePair("imagem", inspecao.foto));
                    nameValuePairs.add(new BasicNameValuePair("latitude", inspecao.latitude));
                    nameValuePairs.add(new BasicNameValuePair("longitude", inspecao.longitude));
                    nameValuePairs.add(new BasicNameValuePair("municipio", inspecao.municipio));
                    nameValuePairs.add(new BasicNameValuePair("naoConformidade", inspecao.naoConformidade));
                    nameValuePairs.add(new BasicNameValuePair("naoConformidadeTexto", inspecao.naoConformidadeTexto));
                    nameValuePairs.add(new BasicNameValuePair("observacao", inspecao.observacao));
                    nameValuePairs.add(new BasicNameValuePair("tecnico", inspecao.tecnico));
                    nameValuePairs.add(new BasicNameValuePair("token", inspecao.token));
                    nameValuePairs.add(new BasicNameValuePair("torre", inspecao.torre));


                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));

                    HttpResponse response = httpClient.execute(httpPost);
                    HttpEntity entity = response.getEntity();

                    if (entity != null) {
                        InputStream instream = entity.getContent();
                        String json = JsonUtil.toString(instream);

                        JSONObject jsonObject = new JSONObject(json);
                        Boolean error = jsonObject.getBoolean("error");

                        if (!error) {
                            String token = jsonObject.getString("token");
                            service.setProtocol(token);
                        }
                    }
                }

                catch (Exception e){
                    Log.d("FIELDCOLLECTOR", e.getMessage());
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mProgressDialog.dismiss();
            ((MainActivity)getActivity()).changeItemMenu(1);

            Toast.makeText(getActivity(), "Envio realizado com sucesso", Toast.LENGTH_SHORT).show();
        }
    }
}
