package br.com.geoambiente.fieldcollector.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by mateus on 13/07/2015.
 */
public class JsonUtil {

    public static String toString(InputStream instream) throws IOException {
        byte[] bytes = new byte[1024];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        int lidos;

        while ((lidos = instream.read(bytes)) > 0)
        {
            baos.write(bytes, 0, lidos);
        }

        return new String(baos.toByteArray());
    }

}
