package br.com.geoambiente.fieldcollector.interfaces;

import android.view.View;

/**
 * Created by mateus on 09/07/2015.
 */
public interface RecycleViewOnClickListenerHack {
    public void onClickListener(View view, int position);
}
