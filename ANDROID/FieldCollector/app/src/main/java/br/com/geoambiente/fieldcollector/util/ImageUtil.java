package br.com.geoambiente.fieldcollector.util;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;

import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mateus on 10/07/2015.
 */
public class ImageUtil {
    private static String namePhoto;

    public static String save(Bitmap bitmap, String name, Boolean compress) throws IOException {
        File folder = new File(Environment.getExternalStorageDirectory() + "/fieldCollector");
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdir();
        }

        if(name == "")
            namePhoto = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss'.png'").format(new Date());
        else
            namePhoto = name + ".jpg";

        File file = new File(folder, namePhoto);

        if (file.exists()) {
            file.delete();
            file = new File(folder, namePhoto);
        }

        FileOutputStream fos = new FileOutputStream(file);

        try {
            if (compress)
                bitmap.compress(Bitmap.CompressFormat.PNG, 60, fos);
            else
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
        }

        catch (Exception e){

        }


        fos.close();

        return folder + File.separator + namePhoto;
    }

    public static Uri getOutputMediaFileUri() {
        File folder = new File(Environment.getExternalStorageDirectory() + "/fieldCollector");
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdir();
        }

        namePhoto = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss'.png'").format(new Date());

        File file = new File(folder, namePhoto);

        if (file.exists()) {
            file.delete();
            file = new File(folder, namePhoto);
        }
        return Uri.fromFile(file);
    }

    public static Bitmap rotate(Bitmap bitmap, int degree) {

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        if(w > h){
            Matrix mtx = new Matrix();
            mtx.postRotate(degree);

            Bitmap bmp = Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);

            return bmp;
        }

        return bitmap;
    }

    public static Bitmap getResizedBitmap(Bitmap bm, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();

        float newHeight = height * ((float)newWidth / width);

        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;

        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();

        // RESIZE THE BITMAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        return resizedBitmap;
    }

    public static Bitmap getBitmapFromCameraData(Intent data, Context context){
        Uri selectedImage = data.getData();
        String[] filePathColumn = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver().query(selectedImage,filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        cursor.close();
        return BitmapFactory.decodeFile(picturePath);
    }

    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth){
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        int reqHeight = (int)(reqWidth * 0.75);

        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;

        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight)
        {
            inSampleSize = Math.round((float)height / (float)reqHeight);
        }
        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth)
        {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float)width / (float)reqWidth);
        }

        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }
}