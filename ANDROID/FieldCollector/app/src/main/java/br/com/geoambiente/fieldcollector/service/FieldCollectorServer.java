package br.com.geoambiente.fieldcollector.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.IBinder;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.geoambiente.fieldcollector.application.CustomApplication;
import br.com.geoambiente.fieldcollector.dao.InspecaoCpflService;
import br.com.geoambiente.fieldcollector.domain.InspecaoCpfl;
import br.com.geoambiente.fieldcollector.util.JsonUtil;

/**
 * Created by mateus on 13/07/2015.
 */
public class FieldCollectorServer extends Service {

    private Context ctx;
    private RequestQueue requestQueue;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ctx = getApplicationContext();
        requestQueue = ((CustomApplication) ctx).getRequestQueue();

        //(new executeInspecaoCpflAsync()).execute();
        try {
            executeInspecaoCpfl();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private class executeInspecaoCpflAsync extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                executeInspecaoCpfl();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    private void executeInspecaoCpfl() throws IOException, JSONException {
        InspecaoCpflService service = new InspecaoCpflService(ctx);
        List<InspecaoCpfl> list = service.getPend();

        for(InspecaoCpfl inspecao : list)
            callWebServiceInspecao(inspecao, service);

        stopSelf();
    }

    private void callWebServiceApcheInspecao(InspecaoCpfl inspecao, InspecaoCpflService service) throws IOException, JSONException {
        String url ="http://130.211.170.236/fieldcollector/MobileApps/PostInspecaoCpfl";

        HttpParams headers = new BasicHttpParams();
        HttpProtocolParams.setContentCharset(headers, "utf-8");

        HttpClient httpClient = new DefaultHttpClient(headers);
        HttpPost httpPost = new HttpPost(url);


        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
        nameValuePairs.add(new BasicNameValuePair("acaoCorretivaPreventiva", inspecao.acaoCorretivaPreventiva));
        nameValuePairs.add(new BasicNameValuePair("dataInspecao", inspecao.dataInspecao));
        nameValuePairs.add(new BasicNameValuePair("imagem", inspecao.foto));
        nameValuePairs.add(new BasicNameValuePair("latitude", inspecao.latitude));
        nameValuePairs.add(new BasicNameValuePair("longitude", inspecao.longitude));
        nameValuePairs.add(new BasicNameValuePair("municipio", inspecao.municipio));
        nameValuePairs.add(new BasicNameValuePair("naoConformidade", inspecao.naoConformidade));
        nameValuePairs.add(new BasicNameValuePair("naoConformidadeTexto", inspecao.naoConformidadeTexto));
        nameValuePairs.add(new BasicNameValuePair("observacao", inspecao.observacao));
        nameValuePairs.add(new BasicNameValuePair("tecnico", inspecao.tecnico));
        nameValuePairs.add(new BasicNameValuePair("token", inspecao.token));
        nameValuePairs.add(new BasicNameValuePair("torre", inspecao.torre));


        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));

        HttpResponse response = httpClient.execute(httpPost);
        HttpEntity entity = response.getEntity();

        if (entity != null)
        {
            InputStream instream = entity.getContent();
            String json = JsonUtil.toString(instream);

            JSONObject jsonObject = new JSONObject(json);
            Boolean error = jsonObject.getBoolean("error");

            if(!error) {
                String token = jsonObject.getString("token");
                service.setProtocol(token);
            }
        }
    }

    private void callWebServiceInspecao(final InspecaoCpfl inspecao, final InspecaoCpflService service) {
        StringRequest request = new StringRequest(Request.Method.POST,
                "http://130.211.170.236/fieldcollector/MobileApps/PostInspecaoCpfl",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Boolean error = jsonObject.getBoolean("error");

                            if(!error) {
                                String token = jsonObject.getString("token");
                                service.setProtocol(token);
                            }

                            stopSelf();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error){
                        stopSelf();
                    }
                }){
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();


                params.put("acaoCorretivaPreventiva", inspecao.acaoCorretivaPreventiva);
                params.put("dataInspecao", inspecao.dataInspecao);
                //params.put("imagem", inspecao.foto);
                params.put("latitude", inspecao.latitude);
                params.put("longitude", inspecao.longitude);
                params.put("municipio", inspecao.municipio);
                params.put("naoConformidade", inspecao.naoConformidade);
                params.put("naoConformidadeTexto", inspecao.naoConformidadeTexto);
                params.put("observacao", inspecao.observacao);
                params.put("tecnico", inspecao.tecnico);
                params.put("token", inspecao.token);
                params.put("torre", inspecao.torre);

                return(params);
            }
        };

        /*request.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));*/

        request.setTag("conn");
        requestQueue.add(request);

    }
}
