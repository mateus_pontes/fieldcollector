package br.com.geoambiente.fieldcollector.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mateus on 09/07/2015.
 */
public class InspecaoCpfl implements Parcelable {

    public int codigo;
    public String token;
    public String torre;
    public String tecnico;
    public String dataInspecao;
    public String dataEnvio;
    public String municipio;
    public String latitude;
    public String longitude;
    public String naoConformidade;
    public String naoConformidadeTexto;
    public String acaoCorretivaPreventiva;
    public String observacao;
    public String foto;
    public String foto2;
    public int sync;

    public InspecaoCpfl(){
        this.codigo = 0;
        this.foto = "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }
}
