package br.com.geoambiente.fieldcollector.dao;

import java.util.List;

/**
 * Created by mateus on 10/07/2015.
 */
public interface IService<T> {
    public List<T> getAll();
    public List<T> getPend();
    public T getById(int codigo);
    public void setProtocol(String token);
    public T saveOrUpdate(T element, boolean novo);
    public void delete(int codigo);
}

