package br.com.geoambiente.fieldcollector.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by mateus on 10/07/2015.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String BANCO_DADOS = "filedcollector";
    private static int VERSAO = 2;

    public DatabaseHelper(Context context) {
        super(context, BANCO_DADOS, null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE inspecaoCpfl(_id INTEGER PRIMARY KEY, token TEXT, torre TEXT, tecnico TEXT, dataInspecao TEXT, dataEnvio TEXT, municipio TEXT, latitude TEXT, longitude TEXT, naoConformidade TEXT, naoConformidadeTexto TEXT, acaoCorretivaPreventiva TEXT, observacao TEXT, foto TEXT, foto2 TEXT, sync INTEGER)";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(oldVersion < 2){
            String sql = "ALTER TABLE inspecaoCpfl ADD COLUMN foto2 TEXT";
            db.execSQL(sql);
        }
    }

    public static SQLiteDatabase  createConnection(Context context) {
        DatabaseHelper helper = new DatabaseHelper(context);
        return helper.getReadableDatabase();
    }
}
