package br.com.geoambiente.fieldcollector.util;

import android.content.Context;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by mateus on 11/07/2015.
 */
public class DateUtil {
    public static Date Now()
    {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();

        return date;
    }

    public static String NowWhitFormatBR(){
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(date);
    }

    public static String GetDateFormat(Context context, Date data) {
        java.text.DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(context);
        return dateFormat.format(data);
    }

    public static Date dateFromString(String dateString) throws ParseException
    {
        if(dateString.equals(""))
            return Now();

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.parse(dateString);
    }
}
