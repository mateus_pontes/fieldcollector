package br.com.geoambiente.fieldcollector.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import br.com.geoambiente.fieldcollector.MainActivity;
import br.com.geoambiente.fieldcollector.R;
import br.com.geoambiente.fieldcollector.dao.InspecaoCpflService;
import br.com.geoambiente.fieldcollector.domain.DomainHelper;
import br.com.geoambiente.fieldcollector.interfaces.IEventsMenu;
import br.com.geoambiente.fieldcollector.util.DateUtil;
import br.com.geoambiente.fieldcollector.util.ImageUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class InspecaoCpflFragment extends Fragment implements IEventsMenu, View.OnClickListener {

    private static final int SELECT_PICTURE = 1;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 2;
    private Uri fileUri;
    private ImageView imageInspecao;
    private ImageView imageInspecao2;
    private EditText torre;
    private EditText tecnico;
    private Spinner naoConformidade;
    private Spinner municipio;
    private EditText acaoCorretivaPreventiva;
    private EditText naoconformidadeTexto;
    private EditText observacao;
    private EditText latitude;
    private EditText longitude;
    private ImageButton btnLocalizar;

    public InspecaoCpflFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity)getActivity()).habilitaItemMenu(new int[]{R.id.menu_camera, R.id.menu_save}, false);
        ((MainActivity)getActivity()).setEventsMenu(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_inspecao_cpfl, container, false);

        imageInspecao = (ImageView) view.findViewById(R.id.imageInspecao);
        //imageInspecao2 = (ImageView) view.findViewById(R.id.imageInspecao2);
        torre = (EditText) view.findViewById(R.id.torre);
        tecnico = (EditText) view.findViewById(R.id.tecnico);
        naoConformidade = (Spinner) view.findViewById(R.id.naoconformidade);
        municipio = (Spinner) view.findViewById(R.id.municipio);
        naoconformidadeTexto = (EditText) view.findViewById(R.id.naoconformidadeTexto);
        acaoCorretivaPreventiva = (EditText) view.findViewById(R.id.acaoCorretivaPreventiva);
        observacao = (EditText) view.findViewById(R.id.observacao);
        longitude = (EditText) view.findViewById(R.id.longitude);
        latitude = (EditText) view.findViewById(R.id.latitude);
        btnLocalizar = (ImageButton) view.findViewById(R.id.btnLocalizar);

        btnLocalizar.setOnClickListener(this);

        int height = ((WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getHeight();

        if(height > 1000)
            imageInspecao.getLayoutParams().height = 400;

        getLocation();

        carregaCampos();

        return view;
    }

    private void carregaCampos() {

        Boolean carrega = ((DomainHelper.mInspecaoCpfl.codigo > 0) || (DomainHelper.mInspecaoCpfl.torre != ""));

        if(carrega){

            if(!DomainHelper.mInspecaoCpfl.foto.equals("")) {
                Bitmap bitmap = BitmapFactory.decodeFile(DomainHelper.mInspecaoCpfl.foto);
                imageInspecao.setImageBitmap(bitmap);
            }

            municipio.setSelection(((ArrayAdapter) municipio.getAdapter()).getPosition(DomainHelper.mInspecaoCpfl.municipio));
            tecnico.setText(DomainHelper.mInspecaoCpfl.tecnico);
            torre.setText(DomainHelper.mInspecaoCpfl.torre);
            latitude.setText(DomainHelper.mInspecaoCpfl.latitude);
            longitude.setText(DomainHelper.mInspecaoCpfl.longitude);
            naoConformidade.setSelection(((ArrayAdapter) naoConformidade.getAdapter()).getPosition(DomainHelper.mInspecaoCpfl.naoConformidade));
            naoconformidadeTexto.setText(DomainHelper.mInspecaoCpfl.naoConformidadeTexto);
            acaoCorretivaPreventiva.setText(DomainHelper.mInspecaoCpfl.acaoCorretivaPreventiva);
            observacao.setText(DomainHelper.mInspecaoCpfl.observacao);
        }
        else
        {
            if(isRegistered()){
                tecnico.setText(getTecnico());
            }
        }
    }

    private void getLocation() {
        Location l = ((MainActivity)getActivity()).getLocation();

        if(l != null){
            longitude.setText("" + l.getLongitude());
            latitude.setText("" + l.getLatitude());
        }
    }


    @Override
    public void takePicture() {
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setIcon(R.mipmap.ic_launcher);
        alert.setTitle("Inspeção de Campo");
        alert.setItems(new String[]{"Tirar Foto", "Minhas Imagens"}, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(which == 0){
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    fileUri = ImageUtil.getOutputMediaFileUri();
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

                    // start the image capture Intent
                    startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                }else{
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent,
                            "Field Collector"), SELECT_PICTURE);
                }
            }
        });
        alert.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Point size = new Point();
        getActivity().getWindowManager().getDefaultDisplay().getSize(size);

        int width = (size.x);
        Bitmap bitmap;
        if (requestCode == SELECT_PICTURE) {

            if(data == null)
                return;

            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            bitmap = BitmapFactory.decodeFile(picturePath);
            bitmap = ImageUtil.getResizedBitmap(bitmap, width);
            imageInspecao.setImageBitmap(bitmap);
            //imageInspecao2.setImageBitmap(bitmap);


            DomainHelper.mInspecaoCpfl.foto = picturePath;
        }
        else if(requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE){
            //bitmap = BitmapFactory.decodeFile(fileUri.getPath());
            bitmap = ImageUtil.decodeSampledBitmapFromFile(fileUri.getPath(), width);
            imageInspecao.setImageBitmap(bitmap);

            DomainHelper.mInspecaoCpfl.foto = fileUri.getPath();
        }
    }

    public String getPath(Uri uri) {
        // just some safety built in
        if( uri == null ) {
            // TODO perform some logging or show user feedback
            return null;
        }
        // try to retrieve the image from the media store first
        // this will only work for images selected from gallery
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        if( cursor != null ){
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        // this is our fallback here
        return uri.getPath();
    }

    @Override
    public void save() {
        boolean error = false;
        if(torre.getText().toString().equals("")){
            torre.setError("Campo Obrigatório");
            error = true;
        }

        if(tecnico.getText().toString().equals("")){
            tecnico.setError("Campo Obrigatório");
            error = true;
        }

        if(longitude.getText().toString().equals("")){
            longitude.setError("Campo Obrigatório");
            error = true;
        }

        if(latitude.getText().toString().equals("")){
            latitude.setError("Campo Obrigatório");
            error = true;
        }

        if(municipio.getSelectedItemPosition() == 0)
        {
            TextView errorText = (TextView)municipio.getSelectedView();
            errorText.setError("Município é Obrigatório");
            errorText.setTextColor(Color.RED);//just to highlight that this is an error
            errorText.setText("Município é Obrigatório");
            error = true;
        }

        if(DomainHelper.mInspecaoCpfl.foto.equals("")){
            Toast.makeText(getActivity(), "Favor registrar a imagem", Toast.LENGTH_SHORT).show();
            error = true;
        }

        if(!error){
            DomainHelper.mInspecaoCpfl.municipio = municipio.getSelectedItem().toString();
            DomainHelper.mInspecaoCpfl.tecnico = tecnico.getText().toString();
            DomainHelper.mInspecaoCpfl.torre = torre.getText().toString();
            DomainHelper.mInspecaoCpfl.dataInspecao = DateUtil.NowWhitFormatBR();
            DomainHelper.mInspecaoCpfl.latitude = latitude.getText().toString();
            DomainHelper.mInspecaoCpfl.longitude = longitude.getText().toString();

            if(naoConformidade.getSelectedItemPosition() > 0)
                DomainHelper.mInspecaoCpfl.naoConformidade = naoConformidade.getSelectedItem().toString();

            DomainHelper.mInspecaoCpfl.naoConformidadeTexto = naoconformidadeTexto.getText().toString();
            DomainHelper.mInspecaoCpfl.acaoCorretivaPreventiva = acaoCorretivaPreventiva.getText().toString();
            DomainHelper.mInspecaoCpfl.observacao = observacao.getText().toString();

            (new InspecaoCpflService(getActivity())).saveOrUpdate(DomainHelper.mInspecaoCpfl, true);
            Toast.makeText(getActivity(), "Inspeção realizada com sucesso!", Toast.LENGTH_SHORT).show();

            setTecnico(DomainHelper.mInspecaoCpfl.tecnico);

            ((MainActivity)getActivity()).changeItemMenu(1);
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        if(id == R.id.btnLocalizar){
            getLocation();
        }
    }

    private String getTecnico() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        return prefs.getString("tecnico", "");
    }

    private void setTecnico(String tecnico) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        prefs.edit().putString("tecnico", tecnico).apply();
    }

    private boolean isRegistered() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        return prefs.contains("tecnico");
    }

    @Override
    public void send() {
       // new sendAsync().execute();
    }


}
