package br.com.geoambiente.fieldcollector.interfaces;

/**
 * Created by mateus on 10/07/2015.
 */
public interface IEventsMenu {
    public void takePicture();
    public void send();
    public void save();
}
