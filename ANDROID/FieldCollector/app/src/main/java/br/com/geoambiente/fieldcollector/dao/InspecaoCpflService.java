package br.com.geoambiente.fieldcollector.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import br.com.geoambiente.fieldcollector.database.DatabaseHelper;
import br.com.geoambiente.fieldcollector.domain.InspecaoCpfl;
import br.com.geoambiente.fieldcollector.util.DateUtil;
import br.com.geoambiente.fieldcollector.util.TokenUtil;

/**
 * Created by mateus on 10/07/2015.
 */
public class InspecaoCpflService implements IService<InspecaoCpfl> {

    private static final String table = "inspecaoCpfl";
    private Context _context;

    public InspecaoCpflService(Context context){
        _context = context;
    }

    @Override
    public List<InspecaoCpfl> getAll() {
        SQLiteDatabase db = DatabaseHelper.createConnection(_context);
        List<InspecaoCpfl> result = new ArrayList<InspecaoCpfl>();
        try {

            Cursor cursor =
                    db.rawQuery("select * from inspecaoCpfl order by _id desc", null);

            cursor.moveToFirst();
            for(int i = 0; i<cursor.getCount(); i++){
                InspecaoCpfl p = new InspecaoCpfl();


                p.codigo = cursor.getInt(cursor.getColumnIndex("_id"));
                p.torre = cursor.getString(cursor.getColumnIndex("torre"));
                p.municipio = cursor.getString(cursor.getColumnIndex("municipio"));
                p.token = cursor.getString(cursor.getColumnIndex("token"));
                p.tecnico = cursor.getString(cursor.getColumnIndex("tecnico"));
                p.dataInspecao = cursor.getString(cursor.getColumnIndex("dataInspecao"));
                p.dataEnvio = cursor.getString(cursor.getColumnIndex("dataEnvio"));
                p.latitude = cursor.getString(cursor.getColumnIndex("latitude"));
                p.longitude = cursor.getString(cursor.getColumnIndex("longitude"));
                p.naoConformidade = cursor.getString(cursor.getColumnIndex("naoConformidade"));
                p.naoConformidadeTexto = cursor.getString(cursor.getColumnIndex("naoConformidadeTexto"));
                p.acaoCorretivaPreventiva = cursor.getString(cursor.getColumnIndex("acaoCorretivaPreventiva"));
                p.observacao = cursor.getString(cursor.getColumnIndex("observacao"));
                p.foto = cursor.getString(cursor.getColumnIndex("foto"));
                p.foto2 = cursor.getString(cursor.getColumnIndex("foto2"));
                p.sync = cursor.getInt(cursor.getColumnIndex("sync"));

                result.add(p);

                cursor.moveToNext();
            }

            return result;
        }

        catch (Exception ex)
        {
            Log.e("UsuaroService", "HasUsuario: " + ex.getMessage());
            return result;
        }

        finally {
            db.close();
        }
    }

    @Override
    public List<InspecaoCpfl> getPend() {
        SQLiteDatabase db = DatabaseHelper.createConnection(_context);
        List<InspecaoCpfl> result = new ArrayList<InspecaoCpfl>();
        try {

            Cursor cursor =
                    db.rawQuery("select * from inspecaoCpfl where sync = -1", null);

            cursor.moveToFirst();
            for(int i = 0; i<cursor.getCount(); i++){
                InspecaoCpfl p = new InspecaoCpfl();


                p.codigo = cursor.getInt(cursor.getColumnIndex("_id"));
                p.torre = cursor.getString(cursor.getColumnIndex("torre"));
                p.municipio = cursor.getString(cursor.getColumnIndex("municipio"));
                p.token = cursor.getString(cursor.getColumnIndex("token"));
                p.tecnico = cursor.getString(cursor.getColumnIndex("tecnico"));
                p.dataInspecao = cursor.getString(cursor.getColumnIndex("dataInspecao"));
                p.dataEnvio = cursor.getString(cursor.getColumnIndex("dataEnvio"));
                p.latitude = cursor.getString(cursor.getColumnIndex("latitude"));
                p.longitude = cursor.getString(cursor.getColumnIndex("longitude"));
                p.naoConformidade = cursor.getString(cursor.getColumnIndex("naoConformidade"));
                p.naoConformidadeTexto = cursor.getString(cursor.getColumnIndex("naoConformidadeTexto"));
                p.acaoCorretivaPreventiva = cursor.getString(cursor.getColumnIndex("acaoCorretivaPreventiva"));
                p.observacao = cursor.getString(cursor.getColumnIndex("observacao"));
                p.foto = cursor.getString(cursor.getColumnIndex("foto"));
                p.foto2 = cursor.getString(cursor.getColumnIndex("foto2"));
                p.sync = cursor.getInt(cursor.getColumnIndex("sync"));


                String imagemBytes = "";
                if(!p.foto.equals("")){
                    File file = new  File(p.foto);

                    Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                    byte[] image = stream.toByteArray();
                    imagemBytes = Base64.encodeToString(image, 0);
                }

                p.foto = imagemBytes;

                imagemBytes = "";
                if(!p.foto2.equals("")){
                    File file = new  File(p.foto2);

                    Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                    byte[] image = stream.toByteArray();
                    imagemBytes = Base64.encodeToString(image, 0);
                }

                p.foto2 = imagemBytes;

                result.add(p);

                cursor.moveToNext();
            }

            return result;
        }

        catch (Exception ex)
        {
            Log.e("UsuaroService", "HasUsuario: " + ex.getMessage());
            return result;
        }

        finally {
            db.close();
        }
    }

    public List<InspecaoCpfl> getTorre(String torre) {
        SQLiteDatabase db = DatabaseHelper.createConnection(_context);
        List<InspecaoCpfl> result = new ArrayList<InspecaoCpfl>();
        try {

            Cursor cursor =
                    db.rawQuery("select * from inspecaoCpfl where torre like '" + torre + "%' order by _id desc", null);

            cursor.moveToFirst();
            for(int i = 0; i<cursor.getCount(); i++){
                InspecaoCpfl p = new InspecaoCpfl();


                p.codigo = cursor.getInt(cursor.getColumnIndex("_id"));
                p.torre = cursor.getString(cursor.getColumnIndex("torre"));
                p.municipio = cursor.getString(cursor.getColumnIndex("municipio"));
                p.token = cursor.getString(cursor.getColumnIndex("token"));
                p.tecnico = cursor.getString(cursor.getColumnIndex("tecnico"));
                p.dataInspecao = cursor.getString(cursor.getColumnIndex("dataInspecao"));
                p.dataEnvio = cursor.getString(cursor.getColumnIndex("dataEnvio"));
                p.latitude = cursor.getString(cursor.getColumnIndex("latitude"));
                p.longitude = cursor.getString(cursor.getColumnIndex("longitude"));
                p.naoConformidade = cursor.getString(cursor.getColumnIndex("naoConformidade"));
                p.naoConformidadeTexto = cursor.getString(cursor.getColumnIndex("naoConformidadeTexto"));
                p.acaoCorretivaPreventiva = cursor.getString(cursor.getColumnIndex("acaoCorretivaPreventiva"));
                p.observacao = cursor.getString(cursor.getColumnIndex("observacao"));
                p.foto = cursor.getString(cursor.getColumnIndex("foto"));
                p.foto2 = cursor.getString(cursor.getColumnIndex("foto2"));
                p.sync = cursor.getInt(cursor.getColumnIndex("sync"));


                String imagemBytes = "";
                /*if(!p.foto.equals("")){
                    File file = new  File(p.foto);

                    Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                    byte[] image = stream.toByteArray();
                    imagemBytes = Base64.encodeToString(image, 0);
                }

                p.foto = imagemBytes;*/

                result.add(p);

                cursor.moveToNext();
            }

            return result;
        }

        catch (Exception ex)
        {
            Log.e("UsuaroService", "HasUsuario: " + ex.getMessage());
            return result;
        }

        finally {
            db.close();
        }
    }

    @Override
    public InspecaoCpfl getById(int codigo) {
        return null;
    }

    @Override
    public void setProtocol(String token) {
        SQLiteDatabase db = DatabaseHelper.createConnection(_context);

        try
        {
            ContentValues values = new ContentValues();
            values.put("sync", 1);
            values.put("dataEnvio", DateUtil.NowWhitFormatBR());

            db.update(table, values, "token = ?", new String[] {String.valueOf(token)});

        }
        finally {
            db.close();
        }
    }

    @Override
    public InspecaoCpfl saveOrUpdate(InspecaoCpfl element, boolean novo) {
        SQLiteDatabase db = DatabaseHelper.createConnection(_context);

        try
        {
            ContentValues values = new ContentValues();
            values.put("municipio", element.municipio);
            values.put("tecnico", element.tecnico);
            values.put("torre", element.torre);
            values.put("dataInspecao", element.dataInspecao);
            values.put("dataEnvio", element.dataEnvio);
            values.put("latitude", element.latitude);
            values.put("longitude", element.longitude);
            values.put("naoConformidade", element.naoConformidade);
            values.put("naoConformidadeTexto", element.naoConformidadeTexto);
            values.put("acaoCorretivaPreventiva", element.acaoCorretivaPreventiva);
            values.put("observacao", element.observacao);
            values.put("foto", element.foto);
            values.put("foto2", element.foto2);

            if(novo)
                values.put("sync", -1);
            else
                values.put("sync", 0);

            if(element.codigo == 0) {
                values.put("token", TokenUtil.generate());
                element.codigo = (int) db.insert(table, null, values);
            }
            else
                db.update(table, values, "_id = ?", new String[] {String.valueOf(element.codigo)});

        }
        finally {
            db.close();
            return element;
        }
    }

    @Override
    public void delete(int codigo) {

    }
}
