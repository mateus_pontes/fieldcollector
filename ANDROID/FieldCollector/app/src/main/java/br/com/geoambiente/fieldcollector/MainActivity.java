package br.com.geoambiente.fieldcollector;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.geoambiente.fieldcollector.application.CustomApplication;
import br.com.geoambiente.fieldcollector.dao.InspecaoCpflService;
import br.com.geoambiente.fieldcollector.domain.DomainHelper;
import br.com.geoambiente.fieldcollector.domain.InspecaoCpfl;
import br.com.geoambiente.fieldcollector.fragments.HomeFragment;
import br.com.geoambiente.fieldcollector.fragments.InspecaoCpflFragment;
import br.com.geoambiente.fieldcollector.fragments.ListaInspecaoCPFLFragment;
import br.com.geoambiente.fieldcollector.interfaces.IEventFilter;
import br.com.geoambiente.fieldcollector.interfaces.IEventsMenu;
import br.com.geoambiente.fieldcollector.service.FieldCollectorServer;
import br.com.geoambiente.fieldcollector.util.ImageUtil;


public class MainActivity extends ActionBarActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, SearchView.OnQueryTextListener {

    private Toolbar mToolbar;
    private Drawer.Result navigationDrawer;
    private FragmentManager mFragmentManager;
    private Fragment mFragment;
    private Menu _menu;
    private GoogleApiClient mGoogleApiClient;
    private IEventsMenu eventsMenu;
    private IEventFilter mIEventFilter;
    private AlarmManager alarmMgr;
    private long tempoRepetir = 10000;
    private PendingIntent alarmIntent;
    private SearchView mSearchView;
    private ProgressBar progressBar;
    private Context ctx;
    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar)findViewById(R.id.toolbar_main);
        mToolbar.setLogo(R.mipmap.ic_launcher);
        mToolbar.setTitle("Field Collector");
        mToolbar.setSubtitle("Geoambiente - SRC Missão");

        setSupportActionBar(mToolbar);
        mFragmentManager = getSupportFragmentManager();

        navigationDrawer = new Drawer()
                .withActivity(this)
                .withToolbar(mToolbar)
                .withDisplayBelowToolbar(true)
                .withTranslucentStatusBar(true)
                .withActionBarDrawerToggleAnimated(true)
                .withDrawerGravity(Gravity.LEFT)
                .withSavedInstance(savedInstanceState)
                .withSelectedItem(0)
                .withFooterDivider(true)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l, IDrawerItem iDrawerItem) {

                        mFragment = null;
                        DomainHelper.mFilter = "";

                        switch (position) {
                            case 1:
                                mFragment = new ListaInspecaoCPFLFragment();
                                break;
                            case 2:
                                break;
                            case 3:
                                break;
                            default:
                                mFragment = new HomeFragment();
                        }

                        if (mFragment != null) {
                            mFragmentManager.beginTransaction().replace(R.id.rl_fragment_container, mFragment).commit();
                        }

                        if (mFragment instanceof ListaInspecaoCPFLFragment) {
                            habilitaItemMenu(new int[]{R.id.menu_inspecaoCpfl, R.id.menu_send, R.id.menu_search}, true);
                        }
                    }
                })
                .build();


        navigationDrawer.addItem(new SectionDrawerItem().withName("Minhas Missões"));
        navigationDrawer.addItem(new PrimaryDrawerItem().withName("Ficha de Inspeção de Campo - CPFL").withIcon(getResources().getDrawable(R.drawable.cpfl)));
        navigationDrawer.addItem(new PrimaryDrawerItem().withName("Mapeamento Unidade Geoambiental - SEMACE").withIcon(getResources().getDrawable(R.drawable.semace)));
        navigationDrawer.addItem(new PrimaryDrawerItem().withName("Despesa de Viagem").withIcon(getResources().getDrawable(R.mipmap.ic_launcher)));;


        mFragment = new HomeFragment();
        mFragmentManager.beginTransaction().replace(R.id.rl_fragment_container, mFragment).commit();

        //agendaEnvioFieldCollectorServer();
    }

    private void agendaEnvioFieldCollectorServer() {

        /*alarmMgr = (AlarmManager)this.getApplicationContext().getSystemService(Context.ALARM_SERVICE);

        Intent it = new Intent(this.getApplicationContext(), FieldCollectorServer.class);
        alarmIntent = PendingIntent.getService(this, 0, it, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());

        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), tempoRepetir, alarmIntent);*/
    }

    public void changeItemMenu(int position){
        navigationDrawer.setSelection(position);
    }

    @Override
    protected void onResume() {
        super.onResume();

        callConnection();
        requestQueue = Volley.newRequestQueue(this);
    }

    private void callConnection() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();

        mGoogleApiClient.connect();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        _menu = menu;

        _menu.findItem(R.id.menu_inspecaoCpfl).setVisible(false);
        _menu.findItem(R.id.menu_camera).setVisible(false);
        _menu.findItem(R.id.menu_save).setVisible(false);
        _menu.findItem(R.id.menu_send).setVisible(false);
        _menu.findItem(R.id.menu_search).setVisible(false);


        SearchManager mSearchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem mMenuItem = menu.findItem(R.id.menu_search);

        mSearchView = (SearchView) mMenuItem.getActionView();
        mSearchView.setSearchableInfo(mSearchManager.getSearchableInfo(getComponentName()));
        mSearchView.setQueryHint("Localizar Torre");
        mSearchView.setOnQueryTextListener(this);

        mSearchView.setQuery("", false);
        mSearchView.clearFocus();
        mSearchView.setIconifiedByDefault(true);

        return true;
    }

    public void handleSearch(Intent intent)    {
        if(Intent.ACTION_SEARCH.equalsIgnoreCase(intent.getAction())){
            String q = intent.getStringExtra(SearchManager.QUERY);

            mToolbar.setTitle(q);
        }
    }

    public void habilitaItemMenu(int[] itemMenu, boolean visibleSearch){
        _menu.findItem(R.id.menu_inspecaoCpfl).setVisible(false);
        _menu.findItem(R.id.menu_camera).setVisible(false);
        _menu.findItem(R.id.menu_save).setVisible(false);
        _menu.findItem(R.id.menu_send).setVisible(false);
        _menu.findItem(R.id.menu_search).setVisible(false);

        for(int item : itemMenu)
            _menu.findItem(item).setVisible(true);


        if(visibleSearch)
            mSearchView.setVisibility(View.VISIBLE);
        else
            mSearchView.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_inspecaoCpfl) {
            DomainHelper.mInspecaoCpfl = new InspecaoCpfl();
            mFragment = new InspecaoCpflFragment();
            if (mFragment != null) {
                _menu.findItem(R.id.menu_inspecaoCpfl).setVisible(false);
                mFragmentManager.beginTransaction().replace(R.id.rl_fragment_container, mFragment).commit();
            }
            return true;
        }
        else if(id == R.id.menu_camera){
            eventsMenu.takePicture();
        }
        else if(id == R.id.menu_save){
            eventsMenu.save();
        }
        else if(id == R.id.menu_send){
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setIcon(R.mipmap.geoambiente);
            alert.setTitle("Inspeção de Campo");
            alert.setItems(new String[]{"Enviar Inspeções", "Obter Inspeções"}, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if(which == 0){
                        eventsMenu.send();
                    }
                    else{
                        new ObtemInspecaoOnline().execute();
                    }
                }
            });
            alert.show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnected(Bundle bundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if(location != null){
            Log.i("LOCATION", "latitude: " + location.getLatitude());
            Log.i("LOCATION", "longitude" + location.getLongitude());
        }
    }

    public Location getLocation(){
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        return location;
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public void setEventsMenu(IEventsMenu mEventsMenu){
        this.eventsMenu = mEventsMenu;
    }

    public void setEventFilter(IEventFilter mEventFilter){this.mIEventFilter = mEventFilter;}

    public void changeInspecao(InspecaoCpfl item) {
        DomainHelper.mInspecaoCpfl = item;

        mFragment = new InspecaoCpflFragment();
        if (mFragment != null) {
            _menu.findItem(R.id.menu_inspecaoCpfl).setVisible(false);
            mFragmentManager.beginTransaction().replace(R.id.rl_fragment_container, mFragment).commit();
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        mFragment = new ListaInspecaoCPFLFragment();
        if (mFragment != null) {
            mFragmentManager.beginTransaction().replace(R.id.rl_fragment_container, mFragment).commit();
        }

        mSearchView.setQuery("", false);
        mSearchView.clearFocus();
        mSearchView.setIconifiedByDefault(true);

        DomainHelper.mFilter = query;

        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    private class ObtemInspecaoOnline extends AsyncTask<Void, Void, Void>{

        private ProgressDialog mProgressDialog = new ProgressDialog(MainActivity.this);
        Integer total = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog.setIcon(R.mipmap.geoambiente);
            mProgressDialog.setTitle("Aguarde");
            mProgressDialog.setMessage("Obtendo dados do servidor");
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            final InspecaoCpflService service = new InspecaoCpflService(MainActivity.this);
            List<InspecaoCpfl> list = service.getAll();

            final StringBuffer torres = new StringBuffer();

            for(InspecaoCpfl ins : list){
                if(torres.length() == 0)
                    torres.append(ins.torre);
                else
                    torres.append(";" + ins.torre);
            }

            final String p = torres.toString();

            StringRequest request = new StringRequest(Request.Method.POST,
                    "http://130.211.170.236/fieldcollector/MobileApps/ObtemTorresServer",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                total = jsonObject.getInt("total");
                                JSONArray torresJson = jsonObject.getJSONArray("torres");

                                InspecaoCpfl inspecaoCpfl;

                                for (int i = 0; i < torresJson.length(); i++) {
                                    JSONObject objeto = torresJson.getJSONObject(i);
                                    inspecaoCpfl = new InspecaoCpfl();

                                    inspecaoCpfl.token = objeto.getString("token");
                                    inspecaoCpfl.torre = objeto.getString("torre");
                                    inspecaoCpfl.dataInspecao = objeto.getString("dataInspecao");
                                    inspecaoCpfl.dataEnvio = objeto.getString("dataInspecao");
                                    inspecaoCpfl.tecnico = objeto.getString("tecnico");
                                    inspecaoCpfl.municipio = objeto.getString("municipio");
                                    inspecaoCpfl.latitude = objeto.getString("latitude");
                                    inspecaoCpfl.longitude = objeto.getString("longitude");
                                    inspecaoCpfl.naoConformidade = objeto.getString("naoConformidade");
                                    inspecaoCpfl.naoConformidadeTexto = objeto.getString("naoConformidadeTexto");
                                    inspecaoCpfl.acaoCorretivaPreventiva = objeto.getString("acaoCorretivaPreventiva");
                                    inspecaoCpfl.observacao = objeto.getString("observacao");

                                    service.saveOrUpdate(inspecaoCpfl, false);
                                }

                                fimProcesso();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener(){
                        @Override
                        public void onErrorResponse(VolleyError error){
                            Toast.makeText(MainActivity.this, "Erro ao obter dados do servidor", Toast.LENGTH_SHORT).show();
                            mProgressDialog.dismiss();
                        }
                    }){
                @Override
                public Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();

                    params.put("query", p);
                    return(params);
                }
            };

        /*request.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));*/

            request.setTag("conn");
            requestQueue.add(request);

            return null;
        }

        private void fimProcesso() {
            mProgressDialog.dismiss();
            Toast.makeText(MainActivity.this, "Total de registro(s) importado: " + total, Toast.LENGTH_SHORT).show();
            changeItemMenu(1);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }
}
