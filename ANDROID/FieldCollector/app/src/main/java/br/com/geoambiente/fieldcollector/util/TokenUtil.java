package br.com.geoambiente.fieldcollector.util;

import java.util.UUID;

/**
 * Created by mateus on 10/07/2015.
 */
public class TokenUtil {
    public static String generate() {
        return UUID.randomUUID().toString();
    }
}
