﻿$(function () {
    var camadas = new (function () {
        this.stateOpen = false;
        this.urlGeoserver = "http://108.59.85.182:8085/geoserver/portal_cidadao/wms?";
        this.wmsLayers = [];
        this.markers = [];

        this.open = function () {
            $(".layerStyle").animate({
                top: "60px",
                height: "250px"
            }, 700, function () {
                camadas.stateOpen = true;
                $("#camadascontent").show();
            });
        },

        this.close = function () {
            $(".layerStyle").animate({
                top: "43px",
                height: "10px"
            }, 700, function () {
                camadas.stateOpen = true;
                $("#camadascontent").hide();
            });
        },

        this.registerEvents = function () {
            $(".layerStyle").hover(
                function () {
                    camadas.open();
                },
                function () {

                }
            );

            $("#btnFechar").click(function () {
                camadas.close();
            })


            //camadas.executeFilter();
            //camadas.loadTableLayers();
        }

        this.loadTableLayers = function () {
            var html = [];

            $.each(camadas.datasource, function (key, value) {
                html.push("<tr>");

                var urlImage = camadas.urlGeoserver + "REQUEST=GetLegendGraphic&amp;VERSION=1.0.0&amp;FORMAT=image/png&amp;WIDTH=20&amp;HEIGHT=20&amp;LAYER=" + value.layer;
                //html.push("<td><img src=" + urlImage + "></td>");

                var id = "chk_" + value.id;
                html.push("<td><input type='checkbox' id=" + id + " onclick='camadas.habilitacamada(this);' />" + value.title);
                html.push("</tr>");
            })

            $("#tblCamada").append(html.join(''));

            $.each(camadas.datasource.filter(function (x) { return x.visible }), function (key, layer) {
                $("#chk_" + layer.id).click();
            })
        },

        this.executeFilter = function (cb) {

            $("#load").show();

            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: __path + "Home/GetInspecaoCpfl",
                data: { inicio: $("#edtDtInicio").val(), fim: $("#edtDtFim").val() },
                success: function (inspecoes) {

                    if (inspecoes.length == 0)
                    {
                        alert('Nenhuma inspeção foi localizada');
                        return
                    }

                    var bound = new google.maps.LatLngBounds();
                    
                    $.each(inspecoes, function (key, inspecao) {
                        var postition = new google.maps.LatLng(parseFloat(inspecao.latitude), parseFloat(inspecao.longitude));

                        var icon = __path + "content/image/marker/inspecao.png";

                        var marker = new google.maps.Marker({
                            position: postition,
                            map: map,
                            icon: icon,
                            inspecao: inspecao
                        });
                        markers.push(markers);

                        google.maps.event.addListener(marker, 'click', function () {

                            var contentString = "<center><h3>Torre: " + this.inspecao.torre + "</h3></center>";

                            var dt = this.inspecao.dataInspecao.replace("/", "").replace("Date(", "").replace(")/", "")
                            dt = parseInt(dt);

                            var d = new Date(dt)

                            contentString += "<b>Data:</b> " + d.toLocaleString().replace(" 00:00:00", "") + "<br>";
                            contentString += "<b>Técnico:</b> " + this.inspecao.tecnico + "<br>";
                            contentString += "<b>Município:</b> " + this.inspecao.municipio + "<br>";
                            contentString += "<b>Não Conformidade:</b> " + this.inspecao.naoConformidade + " - " + this.inspecao.naoConformidadeTexto + "<br>";
                            contentString += "<b>Ação Corretiva/Preventiva:</b> " + this.inspecao.acaoCorretivaPreventiva + "<br><br>";
                            contentString += "<b>Observação:</b> " + this.inspecao.observacao + "<br><br>";

                            if (this.inspecao.foto != "") {
                                var imagem = __path + "content/denuncias/" + this.inspecao.foto;
                                contentString += "<center><img src='" + imagem +"' style='width: 245px;' /></center>";
                            }

                            var infowindow = new google.maps.InfoWindow({
                                content: contentString
                            });

                            infowindow.open(map, this);
                        })

                        bound.extend(marker.getPosition());


                    })

                    map.fitBounds(bound);
                    $("#load").hide();
                },
                error: function (erro) {
                    console.log(erro);
                }
            })
        },

        this.export = function () {
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: __path + "Home/ExportCSV",
                data: { inicio: $("#edtDtInicio").val(), fim: $("#edtDtFim").val() },
                success: function (result) {
                    window.location = __path + "content/ExportCSV/" + result.arquivo;
                },
                error: function (erro) {
                    console.log(erro);
                }
            })
        }

        this.applyLegend = function (ss) {

            $('.money').mask('');
            var totalDivida = 0;

            var acordos = ss.features.filter(function (x) { return x.properties.SEGMENTO == "Acordos" });
            var ajuizados = ss.features.filter(function (x) { return x.properties.SEGMENTO == "Ajuizados" });
            var retail = ss.features.filter(function (x) { return x.properties.SEGMENTO == "Retail" });
            var veiculos = ss.features.filter(function (x) { return x.properties.SEGMENTO == "Veiculos" });

            var valor = 0;
            $.each(acordos, function (key, value) {
                valor = valor + value.properties.DIVIDA;
                totalDivida = totalDivida + value.properties.DIVIDA;
            });
            $("#lblAcordos").text(acordos.length);
            $("#lblAcordosValor").text(valor.toFixed(2));

            valor = 0;
            $.each(ajuizados, function (key, value) {
                valor = valor + value.properties.DIVIDA;
                totalDivida = totalDivida + value.properties.DIVIDA;
            });
            $("#lblAjuizados").text(ajuizados.length);
            $("#lblAjuizadosValor").text(valor.toFixed(2));

            valor = 0;
            $.each(retail, function (key, value) {
                valor = valor + value.properties.DIVIDA;
                totalDivida = totalDivida + value.properties.DIVIDA;
            });
            $("#lblRetail").text(retail.length);
            $("#lblRetailValor").text(valor.toFixed(2));

            valor = 0;
            $.each(veiculos, function (key, value) {
                valor = valor + value.properties.DIVIDA;
                totalDivida = totalDivida + value.properties.DIVIDA;
            });
            $("#lblVeiculos").text(veiculos.length);
            $("#lblVeiculosValor").text(valor.toFixed(2));

            $("#lblTotal").text((acordos.length + ajuizados.length + retail.length + veiculos.length));
            $("#lblTotalValor").text(totalDivida.toFixed(2));

            $('.money').mask('000.000.000.000.000,00', { reverse: true });
        }

        this.habilitacamada = function (ss) {
            map.data.addGeoJson(ss);

            map.data.setStyle(function (feature) {
                var seguimento = feature.getProperty('SEGMENTO');                
                return {
                    icon: getIcon(seguimento),
                };
            });

            //map.data.addListener('click', function (event) {

            //    var html = [];
            //    event.feature.forEachProperty(function (c, p) {
            //        html.push("<b>" + p + "</b>: " + c + "<br>");
            //    })

            //    var anchor = new google.maps.MVCObject();
            //    anchor.set("position", event.latLng);

            //    var infoWindow = new google.maps.InfoWindow();
            //    infoWindow.setContent(html.join(''));
            //    infoWindow.open(map, anchor);
            //})
        },

        this.habilitaHeatmap = function (ss) {
            var latLngs = [];            

            $.each(ss.features, function (key, feature) {
                var geometry = feature.geometry

                var latLng = new google.maps.LatLng(geometry.coordinates[1], geometry.coordinates[0]);
                latLngs.push(latLng);
            })

            heatmap = new google.maps.visualization.HeatmapLayer({
                data: latLngs
            });
            heatmap.setMap(map);
        },

        this.habilitaCluster = function (ss) {
            $.each(ss.features, function (key, feature) {
                var geometry = feature.geometry

                var latLng = new google.maps.LatLng(geometry.coordinates[1], geometry.coordinates[0]);
                var marker = new google.maps.Marker({
                    position: latLng
                });
                markers.push(marker);
            });

            markerCluster.addMarkers(markers);
        }

        this.habilitacamadaDenuncia = function (cb) {
            if (cb.checked) {
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: "Home/GetDenuncias",
                    success: function (denuncias) {

                        var bound = new google.maps.LatLngBounds();

                        $.each(denuncias, function (key, denuncia) {

                            var postition = new google.maps.LatLng(parseFloat(denuncia.latitude), parseFloat(denuncia.longitude));

                            var icon = getIcon(denuncia.tipo);

                            var marker = new google.maps.Marker({
                                position: postition,
                                map: map,
                                icon: icon,
                                denuncia: denuncia
                            });

                            google.maps.event.addListener(marker, 'click', function () {

                                var contentString = "<h3>" + this.denuncia.tipo + "</h3><br>";
                                contentString += "Data: " + this.denuncia.data + "<br><br>";

                                if (this.denuncia.imagem != "")
                                    contentString += "<center><img src='http://108.59.85.182/portalcidadao/Content/denuncias/" + this.denuncia.imagem + "' style='width: 245px;' /></center>";


                                var infowindow = new google.maps.InfoWindow({
                                    content: contentString
                                });

                                infowindow.open(map, this);
                            })

                            bound.extend(marker.getPosition());
                            camadas.markers.push(marker);
                        });

                        map.fitBounds(bound);
                    }
                })
            } else {
                $.each(camadas.markers, function (key, marker) {
                    marker.setMap(null);
                });

                camadas.markers = [];
            }
        }

        this.datasource = [
            {
                id: 4,
                title: "Débito por Segmento",
                layer: "portal_cidadao:lim_municipio_a_geo_wgs84",
                visible: true,
                transparencia: 1
            },
            {
                id: 3,
                title: "Dívida por Estado",
                layer: "lim_bairro_a_geo_wgs84",
                visible: false,
                transparencia: 0.5
            },
            {
                id: 1,
                title: "Mapa de Calor",
                layer: "portal_cidadao:loc_face_quadra_l_geo_wgs84",
                visible: false,
                transparencia: 1
            }
        ]
    })();
    window.camadas = camadas;
});


function getIcon(seguimento) {
    var path = __path + "/Content/image/marker/";

    switch (seguimento.toLowerCase()) {
        case "acordos":
            return path += "acordos.png";
        case "ajuizados":
            return path += "ajuizados.png";
        case "retail":
            return path += "retail.png";
        case "veiculos":
            return path += "veiculos.png";
        
    }

    return path += "nenhum.png";
}