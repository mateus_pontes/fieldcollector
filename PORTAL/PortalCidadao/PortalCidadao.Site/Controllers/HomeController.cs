﻿using PortalCidadao.Site.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace PortalCidadao.Site.Controllers
{
    public class HomeController : Controller
    {
        private PortalCidadaoEntities db = new PortalCidadaoEntities();

        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetDenuncias()
        {
            var denuncias = db.Denuncias.ToList();
            return Json(denuncias, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetInspecaoCpfl(string inicio, string fim) 
        {
            var query = db.InspecaoCpfls.AsQueryable();

            if (inicio != "")
            {
                var _inicio = Convert.ToDateTime(inicio);
                query = query.Where(p => p.dataInspecao.Value >= _inicio);
            }

            if (fim != "")
            {
                var _fim = Convert.ToDateTime(fim);
                query = query.Where(p => p.dataInspecao.Value <= _fim);
            }

            return Json(query.ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ExportCSV(string inicio, string fim)
        {
            var query = db.InspecaoCpfls.AsQueryable();

            if (inicio != "")
            {
                var _inicio = Convert.ToDateTime(inicio);
                query = query.Where(p => p.dataInspecao.Value >= _inicio);
            }

            if (fim != "")
            {
                var _fim = Convert.ToDateTime(fim);
                query = query.Where(p => p.dataInspecao.Value <= _fim);
            }

            var caminho = Server.MapPath("~/Content/ExportCSV/");
            if (!Directory.Exists(caminho))            
                Directory.CreateDirectory(caminho);

            var nomeArquivo = Path.ChangeExtension(
                Path.GetRandomFileName(),
                ".csv"
            );

            var csv = new StringBuilder();
            csv.Append("TORRE;TECNICO;DATA;PROJECAO;LATITUDE;LONGITUDE;MUNICIPIO;FOTO;HIPERLINK;NAO_CONFOR;ACAO_CORRE;OBS;F14;HYPERLINK;NUM_PONTO");
            
            var url = ("http://" + System.Web.HttpContext.Current.Request.Url.Host + (System.Web.HttpContext.Current.Request.Url.Port != 0 ? ":" + System.Web.HttpContext.Current.Request.Url.Port : "") + (System.Web.HttpContext.Current.Request.ApplicationPath == "/" ? System.Web.HttpContext.Current.Request.ApplicationPath : System.Web.HttpContext.Current.Request.ApplicationPath + "/"));

            foreach (var item in query.ToList())
            {
                csv.Append(Environment.NewLine);
                csv.Append(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};;{12};{13}", 
                            item.torre, 
                            item.tecnico, 
                            item.dataInspecao.Value.ToShortDateString(), 
                            "WGS84", 
                            "" + item.latitude,
                            item.longitude, 
                            item.municipio, 
                            item.foto,
                            url + "Content/denuncias/" + item.foto, 
                            item.naoConformidade + " - " + item.naoConformidadeTexto, 
                            item.acaoCorretivaPreventiva, 
                            item.observacao,
                            url + "Content/denuncias/" + item.foto, 
                            item.torre));
            }

           

            System.IO.File.WriteAllText(caminho + nomeArquivo, csv.ToString(), Encoding.UTF8);

            ExportResult result = new ExportResult();
            result.arquivo = nomeArquivo;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private class ExportResult
        {
            public string arquivo;
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
