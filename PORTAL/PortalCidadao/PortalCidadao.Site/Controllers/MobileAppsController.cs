﻿using PortalCidadao.Site.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PortalCidadao.Site.Controllers
{
    public class MobileAppsController : Controller
    {
        private PortalCidadaoEntities db = new PortalCidadaoEntities();

        //
        // GET: /MobileApps/

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult postDenuncia(FormCollection form)
        {
            var result = new ProtocolResult();


            var protocolo = (form["protocolo"].ToString() == "" ? "" : form["protocolo"].ToString());

            var denuncia = db.Denuncias.FirstOrDefault(p => p.protocolo == protocolo);

            if (denuncia == null) { denuncia = new Denuncia(); }

            denuncia.androidkey = form["androidkey"].ToString();
            denuncia.data = form["data"].ToString();
            denuncia.descricao = form["descricao"].ToString();
            denuncia.latitude = form["latitude"].ToString();
            denuncia.longitude = form["longitude"].ToString();
            denuncia.nome = form["nome"].ToString();
            denuncia.tipo = form["tipo"].ToString();

            if (form["imagem"].ToString() != "")
                denuncia.imagem = createImage(form["imagem"].ToString());


            if (denuncia.codigo == 0)
            {
                var protocolox = DateTime.Today.Month.ToString().PadLeft(2, '0') + DateTime.Today.Day.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();
                denuncia.protocolo = protocolox;

                db.Denuncias.Add(denuncia);
            }
            else
            {
                db.Entry(denuncia).State = System.Data.Entity.EntityState.Modified;
            }

            db.SaveChanges();

            result.protocolo = denuncia.protocolo;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult PostInspecaoCpfl(FormCollection form)
        {
            var result = new FieldCollectorResult();

            try
            {
                var token = form["token"].ToString();

                var inspecao = db.InspecaoCpfls.FirstOrDefault(p => p.token == token);

                if (inspecao == null) { inspecao = new InspecaoCpfl(); }

                inspecao.acaoCorretivaPreventiva = form["acaoCorretivaPreventiva"].ToString();
                inspecao.dataInspecao = Convert.ToDateTime(form["dataInspecao"].ToString());

                if (form["imagem"].ToString() != "")
                    inspecao.foto = createImage(form["imagem"].ToString());

                inspecao.latitude = form["latitude"].ToString();
                inspecao.longitude = form["longitude"].ToString();
                inspecao.municipio = form["municipio"].ToString();
                inspecao.naoConformidade = form["naoConformidade"].ToString();
                inspecao.naoConformidadeTexto = form["naoConformidadeTexto"].ToString();
                inspecao.observacao = form["observacao"].ToString();
                inspecao.tecnico = form["tecnico"].ToString();
                inspecao.token = form["token"].ToString();
                inspecao.torre = form["torre"].ToString();

                if (inspecao.codigo == 0)
                    db.InspecaoCpfls.Add(inspecao);
                else
                    db.Entry(inspecao).State = System.Data.Entity.EntityState.Modified;

                result.error = false;
                result.token = inspecao.token;

                db.SaveChanges();
            }

            catch (Exception ex)
            {
                result.error = true;
                result.mensagem = ex.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtemTorresServer(FormCollection form)
        {
            var result = new ObtemTorresServerResult();

            var query = form["query"].ToString().Split(';');

            var torres = db.InspecaoCpfls.Where(p => !query.Contains(p.torre)).ToList();

            result.torres = new List<InspecaoCpflResult>();
            foreach(var i in torres){
                result.torres.Add(new InspecaoCpflResult(){
                    acaoCorretivaPreventiva = i.acaoCorretivaPreventiva,
                    codigo = i.codigo.ToString(),
                    dataInspecao = i.dataInspecao.Value.ToShortDateString(),
                    foto = i.foto,
                    latitude = i.latitude,
                    longitude = i.longitude,
                    municipio = i.municipio,
                    naoConformidade = i.naoConformidade,
                    naoConformidadeTexto = i.naoConformidadeTexto,
                    observacao = i.observacao,
                    tecnico = i.tecnico,
                    token = i.token,
                    torre = i.torre
                });
            }

            result.total = result.torres.Count();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public class ObtemTorresServerResult
        {
            public Int32 total;
            public List<InspecaoCpflResult> torres;
        }

        public class InspecaoCpflResult
        {
            public string acaoCorretivaPreventiva;
            public string codigo;
            public string dataInspecao;
            public string foto;
            public string latitude;
            public string longitude;
            public string municipio;
            public string naoConformidade;
            public string naoConformidadeTexto;
            public string observacao;
            public string tecnico;
            public string token;
            public string torre;
        }

        public class FieldCollectorResult
        {
            public bool error;
            public string token;
            public string mensagem;
        }

        private class ProtocolResult
        {
            public string protocolo;
        }

        private string createImage(string image)
        {
            var bytes = Convert.FromBase64String(image);

            var caminho = Server.MapPath("~/Content/Denuncias/");

            if (!Directory.Exists(caminho))
            {
                Directory.CreateDirectory(caminho);
            }

            var nomeImagem = Path.ChangeExtension(
                Path.GetRandomFileName(),
                ".jpg"
            );

            var imagem = caminho + "\\" + nomeImagem;

            using (var imageFile = new FileStream(imagem, FileMode.Create))
            {
                imageFile.Write(bytes, 0, bytes.Length);
                imageFile.Flush();
            }

            return nomeImagem;
        }

        public static Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

    }
}
