﻿using PortalCidadao.Site.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PortalCidadao.Site.Controllers
{
    public class ApiController : Controller
    {
        private PortalCidadaoEntities db = new PortalCidadaoEntities();

        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetDenuncias()
        {
            var denuncias = db.Denuncias.ToList();
            return Json(denuncias, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
